/*
  *
  * This file is a part of CoreKeyboard.
  * An on-screenkeyboard for C Suite.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, see {http://www.gnu.org/licenses/}.
  *
  */

#include <QFileInfo>
#include <QDir>
#include <QFontDatabase>

#include <cprime/themefunc.h>
#include <cprime/filefunc.h>

#include "settings.h"

settings::settings()
{
	settingsPath = QDir(CPrime::Variables::CC_System_ConfigDir()).filePath("coreapps/coreapps.conf");

	// set some default settings that are user specific
	if (!QFileInfo::exists(settingsPath))
	{
		cSetting = new QSettings("coreapps", "coreapps");
		qDebug() << "Settings file " << cSetting->fileName();

		CPrime::FileUtils::setupFolder(CPrime::FolderSetup::ConfigFolder);

		setDefaultSettings();
	}
	else
	{
		cSetting = new QSettings(settingsPath, QSettings::NativeFormat);
	}
}


settings::~settings()
{
	delete cSetting;
}


void settings::setDefaultSettings()
{
	cSetting->setValue("CoreApps/KeepActivities", true);
	cSetting->setValue("CoreApps/EnableExperimental", false);
	cSetting->setValue("CoreApps/UIMode", AutoDetect);

	if (autoUIMode() == Mobile)
	{
		cSetting->setValue("CoreKeyboard/Layout", en_US_MOBILE);
		cSetting->setValue("CoreKeyboard/Mode", FILL_BOTTOM);
		cSetting->setValue("CoreApps/IconViewIconSize", QSize(56, 56));
		cSetting->setValue("CoreApps/ListViewIconSize", QSize(48, 48));
		cSetting->setValue("CoreApps/ToolsIconSize", QSize(48, 48));
	}
	else
	{
		cSetting->setValue("CoreKeyboard/Layout", en_US);
		cSetting->setValue("CoreKeyboard/Mode", FIXED_SIZE);
		cSetting->setValue("CoreApps/IconViewIconSize", QSize(48, 48));
		cSetting->setValue("CoreApps/ListViewIconSize", QSize(32, 32));
		cSetting->setValue("CoreApps/ToolsIconSize", QSize(24, 24));
	}

	// App specific settings
	// As default settings not exist we should contain to set value for default
	// Add system font to CoreKeyboard
	QFont genFont = QFontDatabase::systemFont(QFontDatabase::GeneralFont);

	if (not genFont.family().count())
	{
		genFont = QFont("Cantarell", 9);
	}
	cSetting->setValue("CoreKeyboard/Font", genFont.family());
}


uiMode settings::autoUIMode() const
{
	int formFactor = CPrime::ThemeFunc::getFormFactor();
	int touchMode  = CPrime::ThemeFunc::getTouchMode();

	if (formFactor == 2)
	{
		return Mobile;
	}
	else if ((formFactor == 1) && (touchMode == 1))
	{
		return Tablet;
	}
	else
	{
		return Desktop;
	}
}


uiMode settings::getUIMode() const
{
	uiMode ui_mode = uiMode(this->getClampedInt("CoreApps/UIMode", 0, ILLEGAL_UI_MODE));

	if (ui_mode == AutoDetect)
	{
		return autoUIMode();
	}
	return ui_mode;
}


int settings::getClampedInt(const QString& s, int min, int exclusive_max) const
{
	int retval = cSetting->value(s).toInt();

	if ((retval < min) || (retval >= exclusive_max))
	{
		retval = min;
	}
	return retval;
}


settings::cProxy settings::getValue(const QString& appNameSlashKey, const QVariant& defaultValue) const
{
	return cProxy{ cSetting, appNameSlashKey, defaultValue };
}
