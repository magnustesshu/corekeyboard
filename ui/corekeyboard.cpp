/*
  *
  * This file is a part of CoreKeyboard.
  * An on-screenkeyboard for C Suite.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, see {http://www.gnu.org/licenses/}.
  *
  */

/*
  *
  * TODO
  *
  * Currently modifier keys are not supported.
  * We have a list of the modifiers in the keymap files.
  * Based on the click (short/long), we should set them into checked/locked state.
  * If the modifier is in locked state, then it will be unlocked upon a subsequent click.
  * If the modifier is in checked state, then it will be release with any mouse click.
  *
  * Record the time when the first click was registered. Then when the key is released,
  * check the time. If the elapsed time is less than the threshold, set it as checked;
  * otherwise, set it as locked. On our keyboard, simultaneous clicks are not needded.
  *
  */

#include "corekeyboard.h"

#include <QX11Info>
#include <X11/extensions/XTest.h>

CoreKeyboard::CoreKeyboard() : QWidget(), smi(new settings)
{
	initSettings();
	loadKeymap();
	relayKeyboard();

	setAttribute(Qt::WA_ShowWithoutActivating);
	setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);
	setAttribute(Qt::WA_X11NetWmWindowTypeDock, true);
	setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));

	correctMode();

	setMouseTracking(true);

	// For screen rotation
	QScreen *scrn = qApp->primaryScreen();

	connect(scrn, &QScreen::availableGeometryChanged, [this, scrn](const QRect& geometry) {
		qDebug() << "orientation change " << geometry;
		correctMode();
	});
}


CoreKeyboard::~CoreKeyboard()
{
	delete smi;
}


void CoreKeyboard::initSettings()
{
	keyboard_layout = keyboardLayout(smi->getClampedInt("CoreKeyboard/Layout", 0, ILLEGAL_LAYOUT));
	keyboard_mode   = keyboardMode(smi->getClampedInt("CoreKeyboard/Mode", 0, ILLEGAL_MODE));
	mAutoSuggest    = smi->getValue("CoreKeyboard/AutoSuggest");
	QFont mFont = smi->getValue("CoreKeyboard/Font");

	setFont(mFont);
}


void CoreKeyboard::loadKeymap()
{
	// Because of the validation we did when loading it, this is memory safe
	QString keymapName = KEYBOARD_LAYOUT_RESOURCES[int(keyboard_layout)];

	QSettings keymapSett(keymapName, QSettings::IniFormat);

	/* All the KeySym to char data ins under the section Data */
	keymapSett.beginGroup("Data");

	/* Load the data to memory */
	Q_FOREACH (QString key, keymapSett.allKeys())
	{
		keyChart[key.toInt()] = keymapSett.value(key).toStringList();
	}

	/* Close the group */
	keymapSett.endGroup();

	Q_FOREACH (QString page, keymapSett.value("Pages").toStringList())
	{
		Page pg;
		Q_FOREACH (QString row, keymapSett.value("Rows").toStringList())
		{
			pg[row] = keymapSett.value(page + "/" + row).toStringList();
		}

		mKeyMap[page.toInt()] = pg;
	}

	QStringList modKeys = keymapSett.value("Modifiers").toStringList();

	Q_FOREACH (QString key, modKeys)
	{
		modifiers << key.toInt();
	}
}


void CoreKeyboard::relayKeyboard()
{
	layout.clear();
	keypress.clear();

	int cols = 0;

	Q_FOREACH (QString row, mKeyMap[mPage].keys())
	{
		cols = (mKeyMap[mPage][row].count() > cols ? mKeyMap[mPage][row].count() : cols);
	}

	/* cols is the maximum number of columns */
	qreal keywidth = 1.0 * width() / cols;

	/* mKeyMap[ mPage ].keys() list the rows */
	qreal keyheight = height() / mKeyMap[mPage].keys().count();

	/* List of the rows of this page */
	QStringList rows = mKeyMap[mPage].keys();

	for (int row = 0; row < rows.count(); row++)
	{
		/* List of all the keys of this row */
		QStringList keys = mKeyMap[mPage][rows.at(row)];

		QList<int> fatKeys;
		Q_FOREACH (QString key, keys)
		{
			if ((key.toInt() > 1000) and (key.toInt() < 2000))
			{
				fatKeys << key.toInt();
			}
		}

		qreal extraSpace = 0, fatSpace = 0;
		if (fatKeys.count())
		{
			extraSpace = 0;
			fatSpace   = (width() - keys.count() * keywidth) / fatKeys.count();
		}

		else
		{
			/* Some rows have lesser number of keys, give a space at the beginning and end */
			extraSpace = (width() - keys.count() * keywidth) / 2;
			fatSpace   = 0;
		}

		for (int x = 0; x < keys.count(); x++)
		{
			qreal xPos   = extraSpace + x * keywidth;
			qreal yPos   = row * keyheight;
			qreal kwidth = keywidth;
			if (fatKeys.contains(keys[x].toInt()))
			{
				kwidth     += fatSpace;
				extraSpace += fatSpace;
			}
			layout.insert(keys[x].toInt(), QRectF(QPointF(xPos, yPos), QSizeF(kwidth, keyheight)));
		}
	}

	/* Scale the font size */
	qreal maxPtSize = qMin(keywidth, keyheight) * 0.4;

	mFont.setPointSize(maxPtSize);
	setFont(mFont);

	repaint();
}


void CoreKeyboard::resizeEvent(QResizeEvent *rEvent)
{
	rEvent->accept();
	relayKeyboard();
}


void CoreKeyboard::mousePressEvent(QMouseEvent *mEvent)
{
	Q_FOREACH (int key, layout.keys())
	{
		keypress[key] = false;

		bool hasKey = false;
		Q_FOREACH (QRectF rect, layout.values(key))
		{
			hasKey |= rect.contains(mEvent->pos());
		}

		if (hasKey)
		{
			int realKey = 0;
			/* Virtual release of the actual key */
			if (key > 50000)
			{
				realKey = key - 50000;
			}

			/* Ctrl: Contains some symbol or text */
			else if (key > 37000)
			{
				realKey = key - 37000;
			}

			/* Fat keys: Remove 1000 to get its actual value */
			else if (key > 1000)
			{
				realKey = key - 1000;
			}

			/* A normal key */
			else
			{
				realKey = key;
			}

			if ((key != 777) and (key != 888) and (key != 999))
			{
				Display *display = QX11Info::display();

				/* If the key value is greater than 50000, press 'Shift' */
				if (key > 50000)
				{
					XTestFakeKeyEvent(display, 50, true, 0);
				}

				/* If the key value is greater than 37000, press 'Ctrl' */
				else if (key > 37000)
				{
					XTestFakeKeyEvent(display, 37, true, 0);
				}

				/* Virtual press of the actual key */
				XTestFakeKeyEvent(display, realKey, true, 0);
			}

			keypress[key] = true;
			if (modifiers.contains(realKey))
			{
				modPress << realKey;
			}
		}
	}

	repaint();
	mEvent->accept();
}


void CoreKeyboard::mouseReleaseEvent(QMouseEvent *mEvent)
{
	Q_FOREACH (int key, layout.keys())
	{
		keypress[key] = false;

		bool hasKey = false;
		Q_FOREACH (QRectF rect, layout.values(key))
		{
			hasKey |= rect.contains(mEvent->pos());
		}

		if (hasKey)
		{
			int realKey = 0;
			/* Virtual release of the actual key */
			if (key > 50000)
			{
				realKey = key - 50000;
			}

			/* Ctrl: Contains some symbol or text */
			else if (key > 37000)
			{
				realKey = key - 37000;
			}

			/* Fat keys: Remove 1000 to get its actual value */
			else if (key > 1000)
			{
				realKey = key - 1000;
			}

			/* A normal key */
			else
			{
				realKey = key;
			}

			/* If the current key release is not a modifier */
			if ((key != 777) and (key != 888) and (key != 999) and (not modifiers.contains(realKey)))
			{
				Display *display = QX11Info::display();

				/* Virtual release of the actual key */
				XTestFakeKeyEvent(display, realKey, false, 0);

				/* If the key value is greater than 50000, release 'Shift' */
				if (key > 50000)
				{
					XTestFakeKeyEvent(display, 50, false, 0);
				}

				/* If the key value is greater than 37000, release 'Ctrl' */
				else if (key > 37000)
				{
					XTestFakeKeyEvent(display, 37, false, 0);
				}

				/* Release all modifiers if any are pressed */
				Q_FOREACH (int key, modPress)
				{
					XTestFakeKeyEvent(display, key, false, 0);
				}

				modPress.clear();
			}

			else
			{
				switch (key)
				{
				case 777:
					hide();
					break;

				case 888:
					mPage = (mPage == 1 ? 2 : 1);
					relayKeyboard();
				}
			}

			mEvent->accept();
		}
	}

	repaint();
	mEvent->accept();
}


void CoreKeyboard::paintEvent(QPaintEvent *pEvent)
{
	QPainter painter(this);

	Q_FOREACH (int key, layout.keys())
	{
		int realKey = 0;
		/* Shift: Contains an alternate text */
		if (key > 50000)
		{
			realKey = key - 50000;
		}

		/* Ctrl: Contains some symbol or text */
		else if (key > 37000)
		{
			realKey = key - 37000;
		}

		/* Fat keys: Remove 1000 to get its actual value */
		else if (key > 1000)
		{
			realKey = key - 1000;
		}

		/* A normal key */
		else
		{
			realKey = key;
		}

		/* A key having value 0, will be our spacer */
		if (key == 0)
		{
			continue;
		}

		Q_FOREACH (QRectF layoutRect, layout.values(key))
		{
			/* Draw the key background: pressed */
			if (keypress.value(key) or modPress.contains(realKey))
			{
				painter.save();
				painter.setPen(Qt::NoPen);
				painter.setBrush(QColor(Qt::darkGray));
				painter.drawRect(layoutRect.adjusted(4, 4, -1, -1));
				painter.restore();
			}

			/* Draw the key background: normal */
			else
			{
				painter.save();
				painter.setPen(Qt::NoPen);
				painter.setBrush(palette().color(QPalette::Window).darker());
				painter.drawRect(layoutRect.adjusted(4, 4, -1, -1));
				painter.setBrush(palette().color(QPalette::Window).darker());
				painter.drawRect(layoutRect.adjusted(2, 2, -3, -3));
				painter.restore();
			}

			/* Draw the key text: pressed */
			painter.save();
			painter.setRenderHints(QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
			QString keyTxt;

			keyTxt = (key > 50000) ? keyChart[realKey].value(1) : keyChart[realKey].value(0);

			if (not keyTxt.contains("/") or (keyTxt == "/"))
			{
				painter.drawText(layoutRect, Qt::AlignCenter, keyTxt);
			}

			else
			{
				QRectF imgRect = layoutRect;
				qreal  size    = qMin(imgRect.width(), imgRect.height()) * 0.5;

				QImage img(keyTxt);
				img = img.scaled(size, size, Qt::KeepAspectRatio, Qt::SmoothTransformation);

				imgRect.setX(imgRect.x() + (layoutRect.width() - img.width()) / 2);
				imgRect.setY(imgRect.y() + (layoutRect.height() - img.height()) / 2);
				imgRect.setSize(QSizeF(size, size));

				painter.drawImage(imgRect, img);
			}

			painter.restore();
		}
	}

	painter.end();

	pEvent->accept();
}


void CoreKeyboard::toggleShowHide()
{
	if (isVisible())
	{
		hide();
	}
	else
	{
		show();
	}
}


void CoreKeyboard::switchLayout()
{
	// Cycle through keyboard layouts
	int temp = int(keyboard_layout);

	if (++temp == ILLEGAL_LAYOUT)
	{
		temp = DEFAULT_LAYOUT;
	}
	keyboard_layout = keyboardLayout(temp);

	loadKeymap();
	relayKeyboard();
}


void CoreKeyboard::switchMode()
{
	// Cycle through keyboard modes
	int temp = int(keyboard_mode);

	if (++temp == ILLEGAL_MODE)
	{
		temp = DEFAULT_MODE;
	}
	keyboard_mode = keyboardMode(temp);

	// TODO - why does this even happen? I would think the user would want to be able to configure both
	// Since changing layout or mode is unlikely to happen often, so giving the most options is fine
	if (smi->getValue("CoreKeyboard/Layout"))
	{
		switchLayout();
	}

	correctMode();

	show();
}


void CoreKeyboard::correctMode()
{
	QSize size = qApp->primaryScreen()->size();

	switch (keyboard_mode)
	{
	case FIXED_SIZE:
		resize(400, 200);
		move(size.width() - width(), size.height() - height());
		qApp->processEvents();
		break;

	case FILL_BOTTOM:
		resize(size.width(), size.height() * .3);
		setGeometry(0, size.height() * .7, size.width(), size.height() * .3);
		break;

	case ILLEGAL_MODE:
		;
	}
}


void CoreKeyboard::changeEvent(QEvent *event)
{
	if ((event->type() == QEvent::ActivationChange) and (!isActiveWindow()))
	{
		setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);
		show();
		event->accept();
	}
	else
	{
		QWidget::changeEvent(event);
		event->accept();
	}
}
