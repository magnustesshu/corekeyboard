/*
  *
  * This file is a part of CoreKeyboard.
  * An on-screenkeyboard for C Suite.
  * Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 3 of the License, or
  * (at your option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, see {http://www.gnu.org/licenses/}.
  *
  */

#pragma once

#include <QFont>
#include <QRect>
#include <QSize>
#include <QSettings>

#include <cprime/variables.h>

enum uiMode
{
	AutoDetect      = 0,
	Desktop         = 1,
	Tablet          = 2,
	Mobile          = 3,
	ILLEGAL_UI_MODE = 4,
};
// Changing the following enums and array are safe, without needing to touch corekeyboard.c, as long as they are internally consistent
enum keyboardMode
{
	DEFAULT_MODE = 0,
	FIXED_SIZE   = 0,
	FILL_BOTTOM  = 1,
	// Ensure this is 1 more than the last one if more modes are added, we use it to cycle through the values
	ILLEGAL_MODE = 2
};
enum keyboardLayout
{
	DEFAULT_LAYOUT = 0,
	en_US          = 0,
	en_US_MOBILE   = 1,
	// One more than the last layout
	ILLEGAL_LAYOUT = 2
};
static const char KEYBOARD_LAYOUT_RESOURCES[][32] = {
	// Must have size {length of keyboardLayout - 1}
	":/resources/en_US.keymap",
	":/resources/en_US_mobile.keymap",
};

class settings {
public:
	explicit settings();
	~settings();

	uiMode getUIMode() const;

	// The user could do all sorts of wacky stuff with the config file, so restrict some values
	// TODO validate the other inputs as well, or ensure they don't cause terrible errors
	int getClampedInt(const QString& s, int min, int exclusive_max) const;

	// A helper structure to convert the result of getValue() to multiple different types
	struct cProxy
	{
		QSettings *cSetting;
		QString   data;
		QVariant  defaultValue;

		operator bool() const
		{
			return cSetting->value(data, defaultValue).toBool();
		}

		operator QString() const
		{
			return cSetting->value(data, defaultValue).toString();
		}

		operator QStringList() const
		{
			return cSetting->value(data, defaultValue).value<QStringList>();
		}

		operator QSize() const
		{
			return cSetting->value(data, defaultValue).toSize();
		}

		operator QFont() const
		{
			return cSetting->value(data, defaultValue).value<QFont>();
		}

		operator QRect() const
		{
			return cSetting->value(data, defaultValue).toRect();
		}
	};

	settings::cProxy getValue(const QString& appNameSlashKey, const QVariant& defaultValue = QVariant()) const;
	QString defaultSettingsFilePath() const;

private:
	QSettings *cSetting;
	QString settingsPath;

	uiMode autoUIMode() const;
	void setDefaultSettings();
};
